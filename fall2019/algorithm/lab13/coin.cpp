#include <iostream>
#include <vector>
using namespace std;

//Function Headers
void printBoard(vector<vector<int>> table);
int calcTable(int up, int left);
void addCoin(int val1, int val2, int amount);

//Gloabl Variables
int size = 9;
vector<vector<int>> table(size, vector<int>(size, 0));
vector<vector<int>> board(size, vector<int>(size, 0));
/*vector<vector<int>> board = {
    {0,1,1},
    {0,1,1},
    {1,1,1},
};*/
int main () {


    cout << "The table " << endl;
    printBoard(table);
    //INSERT COINS
    addCoin(1,1,1);
    addCoin(4,1,2);
    addCoin(5,1,3);
    addCoin(9,1,1);

    addCoin(1,2,2);
    addCoin(3,2,3);
    addCoin(4,2,1);
    addCoin(6,2,2);

    addCoin(2,3,3);
    addCoin(3,3,1);
    addCoin(4,3,2);

    addCoin(7,4,3);
    addCoin(9,4,1);

    addCoin(3,5,2);
    addCoin(4,5,3);
    addCoin(5,5,1);
    addCoin(7,5,2);
    addCoin(9,5,3);

    addCoin(3,6,1);
    addCoin(6,6,2);
    addCoin(8,6,3);
    addCoin(9,6,1);

    addCoin(7,7,2);
    addCoin(8,7,3);
    addCoin(9,7,1);

    addCoin(1,8,2);
    addCoin(3,8,3);
    addCoin(7,8,1);
    addCoin(9,8,2);

    cout << "\nThe game board " << endl;
    printBoard(board);
    int largest;
    int up = size-1; int left = size-1;

    largest = calcTable(up, left);

    cout << "\nNew Table " << endl;
    printBoard(table);

    cout << "Largest Val is: " << largest << endl;

    return 0;
}

int calcTable(int up, int left) {

    if ((up != -1 && left == -1) || (up == -1 && left != -1))
        return 0;

    int ups = calcTable((up-1), left) + board[up][left];
    int lefts = calcTable(up, (left-1)) + board[up][left];
    if (ups > lefts) {
        table[up][left] = ups;
        return ups;
    }
    else {
        table[up][left] = lefts;
        return lefts;
    }

}

void addCoin(int val1, int val2, int amount) {
    int x = val1-1;
    int y = val2-1;

    board[x][y] = amount;
}

void printBoard(vector<vector<int>> table) {
    for (int i = 0; i < table.size(); i++) {
        for (int j = 0; j < table[i].size(); j++) {
            if (table[i][j] < 10)
                cout << " " << table[i][j] << " ";
            else
                cout << table[i][j] << " ";
        }
        cout << endl;
    }
}
