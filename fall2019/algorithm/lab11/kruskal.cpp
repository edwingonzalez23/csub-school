#include <iostream>
#include <vector>
#include <string>
using namespace std;

struct Vertice
{
    int nodeOne, nodeTwo, weight;
    Vertice() {}
    int getNodeOne() {return this->nodeOne;}
    int getNodeTwo() {return this->nodeTwo;}
    int getWeight() {return this->weight;}
};

//Function Headers
void insertVert(vector<Vertice>& vect, int nodeOne, int nodeTwo, int weight);
void startTree(vector<Vertice>& order, vector<Vertice> vect);
void buildHeap(vector<Vertice>& heap);

int numNodes = 5;
int totalCost = 0;
int main() {
    
    vector<Vertice> vertices;
    vector<Vertice> order;

    insertVert(vertices, 0, 1, 5); //a->b 5
    insertVert(vertices, 0, 4, 2); //a->e 2
    insertVert(vertices, 1, 4, 3); //b->e 3
    insertVert(vertices, 0, 2, 7); //a->c 7
    insertVert(vertices, 1, 3, 6); //b->d 6
    insertVert(vertices, 2, 4, 4); //c->e 4
    insertVert(vertices, 4, 3, 5); //e->d 5
    insertVert(vertices, 2, 3, 4); //c->d 4
    
    startTree(order, vertices);
    cout << endl;
    return 0;
}

void startTree(vector<Vertice>& order, vector<Vertice> vect) {
    int counter = 0; 
    while (counter < (numNodes-1)) {
        buildHeap(vect);
        order.push_back(vect[0]);
        vect.erase(vect.begin());
        counter++;
    }

    cout << "\nOrder: " << endl;
    for (int i = 0; i < signed(order.size()); i++) {
        int nodeOne = order[i].getNodeOne();
        int nodeTwo = order[i].getNodeTwo();
        int lineWeight = order[i].getWeight();
        cout << "Connect Node " << (char)(nodeOne+97) << " and Node " << (char)(nodeTwo+97) << " with a line value of " << lineWeight << " " << endl;
        totalCost += lineWeight;
    }

    cout << "\nTotal Cost: " << totalCost;
}

void buildHeap(vector<Vertice>& heap) {
    
    int start = heap.size()/2-1; int size = heap.size();
    
    for (int i = start; i >= 0; i--) {
        
        int childOneIndex = i*2+1;
        int childTwoIndex = i*2+2;
        Vertice childOne = heap[childOneIndex];
        Vertice childTwo = heap[childTwoIndex];
        Vertice parent = heap[i];

        // cout << "Start " << i << " at Parent: " << parent.getWeight() << "\n";
        // cout << "Comparing " << childOne.getWeight() << " " <<  childTwo.getWeight() << endl;

        //Find the min of the two children
        Vertice minChild; int replaceIndex;
        if (childOne.getWeight() > childTwo.getWeight() && (childTwoIndex < size && childOneIndex < size)) {
            minChild = childTwo;
            replaceIndex = childTwoIndex;
        } else if (childTwo.getWeight() > childOne.getWeight() && (childTwoIndex < size && childOneIndex < size)) {
            minChild = childOne;
            replaceIndex = childOneIndex;
        } else if (childOneIndex < size) {
            minChild = childOne;
            replaceIndex = childOneIndex;
        }
        // cout << "Min is " << minChild.getWeight() << endl;

        //Check Parent versus Min Child, Also Recursively Check if More Swaps Needed    
        if (parent.getWeight() > heap[replaceIndex].getWeight()) {
            //cout << "Swapping " << minChild.getWeight() << " and " << parent.getWeight() << endl; 
            heap[replaceIndex] = parent;
            heap[i] = minChild;
            if (i <= start)
                buildHeap(heap);
        }        
    }
}

void insertVert(vector<Vertice>& vect, int nodeOne, int nodeTwo, int weight) {
    Vertice vert; 
    vert.nodeOne = nodeOne;
    vert.nodeTwo = nodeTwo;
    vert.weight = weight;
    vect.push_back(vert);
    //cout << vert.getNodeOne() << " " << vert.getNodeTwo() << " " << vert.getWeight() << " " << endl;
}

