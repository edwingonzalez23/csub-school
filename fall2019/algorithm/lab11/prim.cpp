#include <iostream>
#include <vector>
#include <string>
using namespace std;

struct Edge {
	int node, weight;
    Edge() {}
    int getNode() { return this->node;}
    int getWeight() { return this->weight;}
};

void insert(vector<vector<Edge> >& vect, int weight, int node, int dest);
void buildTree(vector<vector<Edge> >& edge, int startNode); 

vector<int> pathTaken = {0,0,0,0,0};
vector<int> connected = {0,0,0,0,0};
vector<Edge> route;
vector<vector<Edge> > connections;
int totalCost = 0;

int main(int argc, char** argv) {
    //Nodes
    vector<int> nodes = {0,1,2,3,4};//a,b,c,d,e,f
    
    //double vector of nodes
    vector<vector<int> > node;

    vector<vector<Edge> > edge;
    edge.resize(5);
    connections.resize(5);
    insert(edge, 5, 0, 1); //a->b 5
    insert(edge, 7, 0, 2); //a->c 7
    insert(edge, 2, 0, 4); //a->e 5
    insert(edge, 6, 1, 3); //b->d 6
    insert(edge, 5, 4, 3); //e->d 5
    insert(edge, 4, 2, 4); //c->e 4
    insert(edge, 4, 2, 3); //c->d 4
    insert(edge, 3, 4, 1); //e->b 4

    int startNode = atoi(argv[1]);
    buildTree(edge, startNode);

    //All Edges Inserted, Start at a node

    return 0;
}

void buildTree(vector<vector<Edge> >& edge, int begin) {

    //First Scan for Smallest
    /* 
    int minEdge = 100000; int startNode;
    for (int i = 0; i < signed(edge.size()); i++) 
        for (int j = 0; j < signed(edge[i].size()); j++) 
            if (edge[i][j].weight < minEdge) {
                minEdge = edge[i][j].weight;
                startNode = i;
            }
    */
    int startNode = begin;
    cout << "Will Start at Node " << startNode << "\n" << endl;
    Edge start; start.node = startNode; start.weight = 0;
    route.push_back(start);
    pathTaken[startNode] = 1;

    int sNode = startNode; int sMin = 100000; int minNode; int counter = 0;
    for (int j = 0; j < signed(edge[sNode].size()); j++) {
        cout << "Checking Starting Node Node: " << sNode << " to " << edge[sNode][j].node << " with wieght " << edge[sNode][j].weight << endl;
        if (edge[sNode][j].weight < sMin) {
            sMin = edge[sNode][j].weight; 
            minNode = edge[sNode][j].node;
        }
    } 
    
    Edge r; r.node = minNode; r.weight = sMin;    
    route.push_back(r); 
    connections[sNode].push_back(r); counter++; 
    connected[sNode] = 1; connected[minNode] = 1;
    counter++;
    
    cout << "The MinNode connection is: " << startNode << " to " << minNode << " with weight " << sMin << endl;
    cout << "Doing Next Run\n" << endl;
    int done = 0;
    while(done == 0 && counter < 5) {
        int min = 10000000; minNode = 0; int foundParent;
        for (int i = 0; i < signed(edge.size()); i++) {
            for (int j = 0; j < signed(edge[i].size()); j++) {
                /*
                    edge[i][j].node --> Next Potentially Connected Node
                    connected[edge[i][j].node] == 0 --> If not connected, account for it
                    connected[i] == i -> Ensure Parent Node is Connected
                */
                if (connected[edge[i][j].node] == 0 && connected[i] == 1) { 
                    cout << "Checking Node: " << i << " to " << edge[i][j].node << " with wieght " << edge[i][j].weight << endl;
                    if (edge[i][j].weight < min) {
                        min = edge[i][j].weight; 
                        minNode = edge[i][j].node;
                        foundParent = i;
                    }
                }
            } 
        }
        //Create an Edge and Node and Add to Route Vector
        cout << "The MinNode connection is " << foundParent << " to " << minNode << " with weight " << min << endl;
        pathTaken[foundParent] = 1;
        connected[minNode] = 1;
        //order[counter] = foundParent;
        Edge r; r.node = minNode; r.weight = min;     
        route.push_back(r); counter++;
        connections[foundParent].push_back(r);
        min = 1000000; minNode = 10000; foundParent = 0;
        cout << "Doing Next Run\n" << endl;
            
    }
    cout << "-------------------------------------------------------" << endl;
    int rCounter = 0; 
    while (rCounter < 5) {
        if (pathTaken[route[rCounter].node] == 1) {
            cout << "At node " << (char)(route[rCounter].node+97) << " connect ";
            for (int j = 0; j < signed(connections[route[rCounter].node].size()); j++) {
                char append[] = "and";
                if (j > 0)
                    cout << " " << append << " " << (char)(connections[route[rCounter].node][j].node+97) << " ";
                else
                    cout << (char)(connections[route[rCounter].node][j].node+97);

                totalCost += connections[route[rCounter].node][j].weight;
            }
        cout << endl;
        }
        rCounter++;
    } 
    cout << "\nTotal Cost: " << totalCost << endl;
}

void insert(vector<vector<Edge> >& vect, int weight, int node, int dest) {
    struct Edge edge;
	edge.node = dest;
	edge.weight = weight;
    vect[node].push_back(edge);

    struct Edge edge2;
    edge2.node = node;
    edge2.weight = weight;
    vect[dest].push_back(edge2);
}
