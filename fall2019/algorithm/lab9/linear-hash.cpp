#include <vector>
#include <iostream>
using namespace std;
void insert(int val, vector<int>& array,int size);
int linearProbe(int val, vector<int>& array, int index, int& keyComp);
void remove();
void search(int key, vector<int>& array, int size);
int main () {

    //Linear Probing
    int size = 21; 
    vector<int> array(size);
    vector<int> array2(size);
    vector<int> input1 = {7,22,44,27,38,53,40};
    vector<int> input2 = {7,22,44,43,27,89,30,64,85};
    vector<int> keys = {27,40,85};
    //Test Input one!
    for (int i = 0; i < input1.size(); i++)
    {
	insert(input1[i], array, size);
    }
    cout << endl;
    for (int i = 0; i < array.size(); i++) 
    {
	cout << array[i] << " ";
    }

    cout << endl <<  endl;
    cout << "Will search for " << "27,40,85" << endl;
    //Search loop 
    for (int i = 0; i < keys.size(); i++)
    {
	search(keys[i], array, size);
    }
    cout << "------------------------------------------------------------------------"<< endl;
    //test input two!
    for (int i = 0; i < input2.size(); i++)
    {
	insert(input2[i], array2, size);
    }
    cout << endl;
    for (int i = 0; i < array2.size(); i++) 
    {
	cout << array2[i] << " ";
    }

    cout << endl <<  endl;
    cout << "Will search for " << "27,40,85" << endl;
    //Search loop 
    for (int i = 0; i < keys.size(); i++)
    {
	search(keys[i], array2, size);
    }

    return 0;
}

void insert (int val, vector<int>& array, int size) {
    int index = val % size;
    //cout << val << " mod " << 21 << " = " << index;
    //cout << endl;
    if (array[index] == 0) {
	array[index] = val;
    } else {
	//Conduct a Linear Insertion
	index++;
	int done; int exit = 0;
	if (index > (size-1) || index == 0) {
	    index = 0;
	    done = size-1;
	}   
	else
	    done = index-1; 
	while(exit != 1) {
	    if (array[index] == 0) {
		array[index] = val;
		exit = 1;
	    } else {
		index++;
	    }
	    //Check out of bounds
	    if (index > (size-1))
		index = 0;
	    //exit
	    if (index == done) {
		cout << "Array too small! Resize!" << endl;
		exit = 1;
	    }
	}
    }
}

void search (int key, vector<int>& array, int size) {
    int index = key % size;
    int keyComp = 1;
    cout << "Looking for " << key << " at "<< index;
    cout << endl;
    if (array[index] == key) {
	cout << "Found at: " << index << ". Num Key Comparisons: " << keyComp << endl;
    } else {
	index++; 
	int done; int exit = 0;
	if (index > (size-1) || index == 0) {
	    index = 0;
	    done = size-1;
	}   
	else
	    done = index-1; 
	while(exit != 1) {
	    if (array[index] == key) {
		keyComp++;
		cout << "Found at: " << index << ". Num Key Comparisons: " << keyComp << endl;
		exit = 1;
	    } else {
		index++;

	    }
	    //Check out of bounds
	    if (index > (size-1))
		index = 0;
	    //exit
	    if (index == done) {
		cout << "Not Found!" << " Num Key Comparisons: " << keyComp << endl;
		exit = 1;
	    }
	    keyComp++;
	}
    }
    cout << endl;
}
void remove () {

}
