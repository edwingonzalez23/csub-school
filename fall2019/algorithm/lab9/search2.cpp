#include <iostream>
#include <vector>
using namespace std;
void printVector();
int main () {
    cout << "Strings: " << endl;
    printVector();
    vector<char> longS = {'t','h','i','s',' ','i','s',' ','a',' ','t','e','s','t',' ','t','e','x','t'};
    vector<char> shortS = {'t','e','s','t'};
    vector<int> found; vector<int> matches; int shortSize = shortS.size();
    cout << "String Length: " << longS.size() << endl;
    int comp = 0; int scomp = 0; int fcomp = 0;
    int pos = shortS.size()-1; 
    int exitPos = longS.size()-1;
    //Update Shift Table
    vector<int> table(26, shortSize);
    bool matchingLastLetter = false;
    for (int i = 0; i < shortSize; i++)
    {
	if (!matchingLastLetter && i < shortSize-1 && shortS[i] == shortS[shortSize-1]) //Check if matching last letter
	    matchingLastLetter = true;
	if (i == shortSize-1 && !matchingLastLetter) { //If no matching last letter, set skip to full length for final letter
	    int skipAmount = shortSize;
	    int tableIndex = int(shortS[i])-97;
	    table[tableIndex] = skipAmount;
	    break;
	}

	if (i == shortSize-1 && matchingLastLetter) { //if matching last letter, keep a previous value for this letter
	    break;
	}

	int skipAmount = shortSize-i-1; //new table value

	int tableIndex = int(shortS[i])-97; //grab table index
	table[tableIndex] = skipAmount;
    }
    //Print Shift Table
    cout << "Shift Table" << endl;
    for (int i = 0; i < table.size(); i++)
    {
	std::cout << table[i];
    }
    cout << "\n" << endl;
    int tmp = shortSize-1; int sub = 0; int counter = 0;
    //Start word search
    while (pos <= exitPos) {
	if (counter == shortSize) {
		pos = pos + shortSize-1;
		counter = 0;
	} else if (shortS[tmp] == longS[pos - sub]) {
	    matches.push_back(pos-sub);
	    comp++; scomp++; counter++;
	    tmp--; sub++;
	    if (sub == shortSize-1) {
		found.push_back(pos-sub);
	    }
	} else {
	    comp++; fcomp++;
	    //Use table to skip
	    char letter = longS[pos];
	    int skip;
	    if (letter == ' ')
		skip = shortSize;
	    else 
		skip = table[(int(letter)-97)];
	    pos = pos+skip;
	    tmp = shortSize-1; sub = 0;
        counter = 0;
	}
    }

    cout << "Number of Comparisons: " << comp << endl;
    cout << "Number of Successful Comparisons: " << scomp << endl;
    cout << "Number of Failed Comparisons: " << fcomp << endl;
    cout << endl;
    cout << "Matches at Index: ";
    for (int i = 0; i < matches.size(); i++)
    {
	cout << " " << matches[i];
    }
    cout << endl;
    int numTotal = 0;
    for (int i = 0; i < found.size(); i++)
    {
	cout << "Entire Word found at starting Index: " << found[i] << endl;
	numTotal++;
    }
    cout << "Total Matches: " << numTotal << endl;
    return 0;
}
void printVector() {
    vector<char> vec = {'t','h','i','s',' ','i','s',' ','a',' ','t','e','s','t',' ','t','e','x','t'};
    vector<char> vec2 = {'t','e','s','t'};
    for (int i = 0; i < vec.size(); i++) {
	cout << vec[i];
    }
    cout << " VS" << endl;
    for (int i = 0; i < vec2.size(); i++) {
	cout << vec2[i];
    }
    cout << endl;
    cout << endl;
}
