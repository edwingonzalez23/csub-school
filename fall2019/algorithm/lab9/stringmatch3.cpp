#include <stdio.h>
#include <iostream>
#include <vector>
using namespace std;
void printVector();
//global variables
int total, correct, wrong = 0;
int longSize = 24; int shortSize = 7;
vector<int>found; vector<int>matches;
void stringMatch(int array[], int string[]) {
    for (int i = 0; i < longSize; i++) {
	int good = 0;
	//string comparison 
	for (int j = 0; j < shortSize; j++) {
	    if (i > (longSize - shortSize))
		break;
	    if (string[j] == array[i+j]) {
		correct++;
		good++;
		matches.push_back(i+j);
		if (good == shortSize) {
		    //printf("match at pos: %d\n", i);
		    found.push_back(i);
		}
	    } else{
		wrong = wrong + 1;
		break;
	    }
	}
    }
    total = correct + wrong;
    //  for (int i = 0; i < 30; i++) {
    //	printf("%d ", array[i]);
    //	if ((i % 6) == 0 && i!=0 && i!=30)
    //	    printf("\n");
    //   }
    //    printf("\n");
}

int main () {
    int array[30] = 
    {1,0,0,1,0,1,
	0,1,0,0,1,0,
	0,1,1,0,0,1,
	0,1,1,1,0,0,
	1,0,1,1,0,0};

    int longS[24] = {int('t')-97,int('h')-97,int('i')-97,int('s')-97,int(' ')-32,int('i')-97,int('s')-97,int(' ')-32,int('a')-97,int(' ')-32,int('s')-97,int('i')-97,int('m')-97,int('p')-97,int('l')-97,int('e')-97,int(' ')-32,int('e')-97,int('x')-97,int('a')-97,int('m')-97,int('p')-97,int('l')-97, int('e')-97};

    cout << "Long String Size: " << longSize << endl;
    cout << "Using Brute Force: " << longSize << endl; cout << endl;
    cout << "Strings: " << endl;
    printVector();

    int shortS[7] = {int('e')-97,int('x')-97,int('a')-97,int('m')-97,int('p')-97,int('l')-97,int('e')-97};
    int tarray[6] = {0,0,0,0,1,1};

    int string[6] = {0,0,1,0,1,1};

    //stringMatch(array, string);
    stringMatch(longS, shortS);
    printf("Number of Comparisons: %d\n", total );
    printf("Number of Succesful Comparisons: %d\n", correct );
    printf("Number of Failed Comparisons: %d\n", wrong );
    cout << endl;
    cout << "Matches at Index: ";
    for (int i = 0; i < matches.size(); i++)
    {
	cout << " " << matches[i];
    }
    cout << endl;
    int numTotal = 0;
    for (int i = 0; i <found.size(); i++)
    {
	cout << "Entire Word found at starting Index: " <<found[i] << endl;
	numTotal++;
    }
    cout << "Total Matches: " << numTotal << endl;
    return 0;
}

void printVector() {
    vector<char> vec = {'t','h','i','s',' ','i','s',' ','a',' ','s','i','m','p','l','e',' ','e','x','a','m','p','l','e'};
    vector<char> vec2 = {'e','x','a','m','p','l','e'};
    for (int i = 0; i < vec.size(); i++) {
	cout << vec[i];
    }
    cout << " VS" << endl;
    for (int i = 0; i < vec2.size(); i++) {
	cout << vec2[i];
    }
    cout << endl;
    cout << endl;
}


