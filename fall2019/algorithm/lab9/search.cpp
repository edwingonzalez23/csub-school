#include <iostream>
#include <vector>
#include <string>
using namespace std;
int main () {
    int alphSize = 26;
    //vector<int> table = {5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5};
    vector<int> table(26,5);
    vector<char> longS = {'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','b'};
    vector<char> shortS = {'a','a','a','a','b'};
    int comp = 0; int scomp = 0; int fcomp = 0; int tmp;
    int pos = shortS.size()-1; //5
    int exitPos = longS.size()-1; //17
    //Update Shift Table
    int shortSize = shortS.size();
    char last = shortS[shortSize-1];
    bool matchingLastLetter = false;
    for (int i = 0; i < shortSize; i++)
    {
        if (!matchingLastLetter && i < shortSize-1 && shortS[i] == shortS[shortSize-1]) //Check if matching last letter
            matchingLastLetter = true;
        if (i == shortSize-1 && !matchingLastLetter) { //If no matching last letter, set skip to full length for final letter
            int skipAmount = shortSize;
            int tableIndex = int(shortS[i])-97;
            table[tableIndex] = skipAmount;
            break;
        }

        if (i == shortSize-1 && matchingLastLetter) { //if matching last letter, keep a previous value for this letter
            break;
        }
            
        int skipAmount = shortSize-i-1; //new table value
        
        int tableIndex = int(shortS[i])-97; //grab table index
        table[tableIndex] = skipAmount;
        

    }
    //Print Shift Table
    for (int i = 0; i < table.size(); i++)
    {
        cout << table[i];
    }
    cout << endl;
    while (pos <= exitPos) {
        tmp = pos;
        if (shortS[tmp] == longS[tmp]) {
            comp++; scomp++;
            tmp--;
        } else {
            comp++; fcomp++;
            //Use table to skip
            char letter = longS[tmp];
            int skip = table[(int(letter)-97)];
            pos = pos+skip;
        }
    }

    cout << "Number of Comparisons: " << comp << endl;
    cout << "Number of Successful Comparisons: " << scomp << endl;
    cout << "Number of Failed Comparisons: " << fcomp << endl;

    return 0;
}