#include <vector>
#include <iostream>
using namespace std;
void insert(int val, vector<int>& array,int size);
int linearProbe(int val, vector<int>& array, int index, int& keyComp);
void remove();
void search(int key, vector<int>& array, int size);
float largest = 0; int largestIndex = 0; int largestVal = -1;
int average = 0;
int main () {

    //Linear Probing
    int size = 11; 
    vector<int> array(size,-1);
    vector<int> input1 = {30,20,56,75,31,19};
    vector<int> keys = {30,20,56,75,31,19};
    //Test Input one!
    for (int i = 0; i < input1.size(); i++)
    {
		insert(input1[i], array, size);
    }
    cout << endl;
    for (int i = 0; i < array.size(); i++) 
    {
		cout << array[i] << " ";
    }

    cout << endl <<  endl;
    cout << "Will search for ";
    for (int i = 0; i < input1.size(); i++)
    {
	    cout << keys[i] << " ";
    }
    cout << endl;
    //Search loop 
    for (int i = 0; i < keys.size(); i++)
    {
	search(keys[i], array, size);
    }
	cout << endl;
	cout << "Largest Comparison: " << largest << " at index " << largestIndex << endl;
	cout << "When Looking for: " << largestVal << endl;
    cout << "Total Comparisons: " << average << endl;
    cout << "Average Comparisons " << average/(float)keys.size() << endl;
	
    return 0;
}

void insert (int val, vector<int>& array, int size) {
    int index = val % size;
    //cout << val << " mod " << 21 << " = " << index;
    //cout << endl;
    if (array[index] == -1) {
	array[index] = val;
    } else {
	//Conduct a Linear Insertion
	index++;
	int done; int exit = 0;
	if (index > (size-1) || index == 0) {
	    index = 0;
	    done = size-1;
	}   
	else
	    done = index-1; 
	while(exit != 1) {
	    if (array[index] == -1) {
		array[index] = val;
		exit = 1;
	    } else {
		index++;
	    }
	    //Check out of bounds
	    if (index > (size-1))
		index = 0;
	    //exit
	    if (index == done) {
		cout << "Array too small! Resize!" << endl;
		exit = 1;
	    }
	}
    }
}

void search (int key, vector<int>& array, int size) {
    int index = key % size;
    int keyComp = 1;
    cout << "Looking for " << key << " at "<< index;
    cout << endl;
	if (array[index] == -1) {
		cout << "Not Found!" << " Num Key Comparisons: " << keyComp << endl;
		//cout << "Average Key cmp before: " << average << ". Current Key cmp: " << keyComp << " " << endl;
		average += keyComp;
		//cout << "Average Key cmp after: " << average << ". Current Key cmp: " << keyComp << " " << endl;
		if (keyComp > largest) {
            largest = keyComp;
            largestIndex = index;
			largestVal = array[index];
        }
	} else if (array[index] == key) {
		cout << "Found at: " << index << ". Num Key Comparisons: " << keyComp << endl;
		if (keyComp > largest) {
            largest = keyComp;
            largestIndex = index;
			largestVal = array[index];
        }
		//cout << "Average Key cmp before: " << average << ". Current Key cmp: " << keyComp << " " << endl;
		average += keyComp;
		//cout << "Average Key cmp after: " << average << ". Current Key cmp: " << keyComp << " " << endl;
		
    } else {
		index++; 
		int done; int exit = 0;
		if (index > (size-1) || index == 0) {
			index = 0;
			done = size-1;
		}   
		else
			done = index-1; 
		while(exit != 1) {
			if (array[index] == key) {
				keyComp++;
				cout << "Found at: " << index << ". Num Key Comparisons: " << keyComp << endl;
				if (keyComp > largest) {
					largest = keyComp;
					largestIndex = (keyComp - index)-1;
					largestVal = array[index];
				}
				exit = 1;
			} else {
				index++;
			}
			//Check out of bounds
			if (index > (size-1))
				index = 0;
			//exit
			if (index == done) {
				cout << "Not Found!" << " Num Key Comparisons: " << keyComp << endl;
				exit = 1;
			}
			if (keyComp > largest) {
				largest = keyComp;
				largestIndex = index;
				largestVal = array[index];
			}
			
			keyComp++;
			
		}
		int tmpKeyComp = (keyComp-1);
		//cout << "Average Key cmp Before: " << average << ". Current Key cmp:  " << tmpKeyComp << " " << endl;
		average += tmpKeyComp;
		//cout << "Average Key cmp After: " << average << ". Current Key cmp:  " << keyComp-1 << " " << endl;
    }
    cout << endl;
}
void remove () {

}
