#include <vector>
#include <iostream>
using namespace std;
void insert(int val, vector<vector<int> >& array,int size);
int linearProbe(int val, vector<vector<int> >& array, int index, int& keyComp);
void remove();
void search(int key, vector<vector<int> >& array, int size);
float largest = 0; int largestIndex = 0;
int average = 0;
int main () {

    //Linear Probing
    int size = 11; 
    //Vector Of Vectors for chaining 
    vector<vector<int> > chain(size, vector<int>(1));

    vector<int> input1 = {30,20,56,75,31,19};
    vector<int> keys = {30,20,56,75,31,19};
    //Test Input one!
    for (int i = 0; i < input1.size(); i++)
    {
	    insert(input1[i], chain, size);
    }
    cout << endl;
    for (int i = 0; i < chain.size(); i++) 
    {
	for (int j = 0; j < chain[i].size(); j++)
	    cout << i << "--> " << chain[i][j] << " ";    
	cout << endl;
    }

    cout << endl <<  endl;
    cout << "Will search for ";
    for (int i = 0; i < input1.size(); i++)
    {
	    cout << input1[i] << " ";
    }
    cout << endl;
    //Search loop 
    for (int i = 0; i < keys.size(); i++)
    {
	    search(keys[i], chain, size);
    }
    cout << endl;
    cout << "Largest Comparison: " << largest << " at index " << largestIndex << endl;
    cout << "Total Comparisons: " << average << endl;
    cout << "Average Comparisons " << average/(float)keys.size() << endl;
    return 0;
}

void insert (int val, vector<vector<int> >& chain, int size) {
    int index = val % size;
    // cout << val << " mod " << 21 << " = " << index;
    // cout << endl;
    if (chain[index][0] == 0) {
	chain[index][0] = val;
    } else {
	//Conduct a Chain Insertion 
	chain[index].push_back(val); 
    }
}

void search (int key, vector<vector<int> >& chain, int size) {
    int index = key % size;
    int keyComp = 1;
    cout << "Looking for " << key << " at "<< index;
    cout << endl;
    if (chain[index][0] == key) {
	cout << "Found at: " << index << ". Num Key Comparisons: " << keyComp << endl;
        if (keyComp > largest) {
            largest = keyComp;
            largestIndex = index;
        }
        average += keyComp;
            
    } else {
        //Search 
        int subIndex = -1;
        for (int i = 1; i < chain[index].size(); i++)
        {
            keyComp++;
            if (chain[index][i] == key) {
            subIndex = i;
            break;
            }
        }
        if (subIndex != -1)
            cout << "Found at: " << index << "." << subIndex << ". Num Key Comparisons: " << keyComp << endl;
        else
            cout << "Not Found!" << " Num Key Comparisons: " << keyComp << endl;
        
        if (keyComp > largest) {
            largest = keyComp;
            largestIndex = index;
        }
        average += keyComp;
    }
    cout << endl;
}
void remove () {

}
